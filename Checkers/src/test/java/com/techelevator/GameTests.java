package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GameTests {

    private static final int BOARD_ROWS = 8;
    private static final int BOARD_COLS = 8;
    private Game game;
    private CheckerBoard board;
    private int expectedPieces;

    @Before
    public void setup() {
        game = new Game(BOARD_ROWS, BOARD_COLS);
        board = game.getCheckerBoard();
        expectedPieces = game.getStartingPiecesPerPlayer() * 2;
    }

    @Test
    public void game_should_add_pieces_to_checkerboard_when_constructed() {
        int actualPieces = 0;
        for (int row = 0; row < board.getNumberOfRows(); row++) {
            for (int col = 0; col < board.getNumberOfColumns(); col++) {
                if (board.getPiece(row, col) != null) {
                    actualPieces++;
                }
            }
        }
        Assert.assertEquals("Game created a starting board with a different number of pieces than expected",
                expectedPieces, actualPieces);
    }

}
