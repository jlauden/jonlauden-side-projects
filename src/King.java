public class King extends Gamepiece{

    public King(Checker checkerToBecomeKing) {
        super(checkerToBecomeKing.getHorizontalPosition(),
                checkerToBecomeKing.getVerticalPosition(),
                checkerToBecomeKing.getColor());
    }
}
