package com.techelevator;

/*
 * Communicates with user via menu object
 * Coordinates loading and saving of games
 * Instantiates and directs game object
 *
 */

public class CheckersCLI {

    private static final String CHOICE_NEW_GAME = "1";
    private static final String CHOICE_LOAD_GAME = "2";
    private static final String CHOICE_QUIT = "3";
    private Menu menu;
    private Game game;

    public CheckersCLI(Menu menu) {
        this.menu = menu;
    }

    public void run(){

        menu.displayWelcomeMessage();

        switch (menu.getStartMenuChoiceFromUser()) {

            case CHOICE_NEW_GAME:
                int[] boardSize = menu.getBoardSizeFromUser();
                int rows = boardSize[0];
                int cols = boardSize[1];
                game = new Game(rows, cols);

                menu.displayBoard(game.getCheckerBoard());

            case CHOICE_LOAD_GAME:
                //

            case CHOICE_QUIT:
                // Program ends
        }

    }



    public static void main(String[] args) {
        Menu menu = new Menu();
        CheckersCLI cli = new CheckersCLI(menu);
        cli.run();

    }
}
