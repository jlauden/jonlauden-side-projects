package com.techelevator;

import java.util.Scanner;

/**
 * Responsible for applying the rules of the game such as
 *      checking if an attempted move is valid,
 *      remembering whose turn it is,
 *      win/loss conditions,
 *      Initial setup of the board
 *
 * This allows for the possibility of different types of games being implemented without rewriting the gameboard
 */

public class Game {

    private static final int STARTING_PIECES_PER_PLAYER = 12;
    private static final String OPPONENT_PIECE_COLOR = "Red";
    private static final String PLAYER_PIECE_COLOR = "Black";

    private CheckerBoard checkerBoard;

    public Game(int boardRows, int boardCols) {
        checkerBoard = new CheckerBoard(boardRows, boardCols);
        placePiecesForNewGame();
    }

    public void placePiecesForNewGame() {
        int playerPiecesPlaced = 0;
        int opponentPiecesPlaced = 0;

        // Starting at top moving left to right and downward, place opponent pieces
        for (int row = 0; row < checkerBoard.getNumberOfRows() / 2; row++) {
            for (int col = 0; col < checkerBoard.getNumberOfColumns(); col++) {
                if (opponentPiecesPlaced < STARTING_PIECES_PER_PLAYER && isValidSquare(row, col)) {
                    checkerBoard.placePiece(new Checker(OPPONENT_PIECE_COLOR), row, col);
                    opponentPiecesPlaced++;
                }
            }
        }
        // Starting at the bottom moving right to left and upward, place player pieces
        for (int row = checkerBoard.getLastRowIndex(); row > 0; row--) {
            for (int col = checkerBoard.getLastColumnIndex(); col > 0; col--) {
                if (playerPiecesPlaced < STARTING_PIECES_PER_PLAYER && isValidSquare(row, col)) {
                    checkerBoard.placePiece(new Checker(PLAYER_PIECE_COLOR), row, col);
                    playerPiecesPlaced++;
                }
            }
        }
    }

    public boolean isValidSquare(int row, int col) {
        // ToDo:  Write this method
        return true;
    }

    public CheckerBoard getCheckerBoard() {
        return checkerBoard;
    }

    public int getStartingPiecesPerPlayer() {
        return STARTING_PIECES_PER_PLAYER;
    }

//    private Scanner scanner = new Scanner(System.in);
//
//
//
//    public String readBoardPositionFromUser() {
//        String position = "";
//        int row;
//        char column;
//        boolean validPosition = false;
//
//        while(!validPosition) {
//
//            // Temporary
//            System.out.println("What is your next move?");
//            System.out.println("Checker to move: ");
//
//
//            position = scanner.nextLine();
//
//            //try {
//            //    column = char
//            //      equalsignorecase
//            //}
//
//            try {
//                row = Integer.parseInt(position.substring(1));
//            } catch (StringIndexOutOfBoundsException e) {
//                System.out.println("Please try again, row may not have been given");
//                continue;
//            } catch (NumberFormatException e) {
//                System.out.println("Please try again, row should be a number");
//                continue;
//            }
//            validPosition = true;
//
//        }
//        return position;
//    }

}
