package com.techelevator;

public class Checker extends GamePiece{

    private final String color;
    //private boolean king = false;

    public Checker(){
        color = "red";
    }

    public Checker(String color) {
        super(color);
        this.color = color;
    }

    public Checker(Checker checkerToCopy) {
        this.color = checkerToCopy.color;
    }

//    public boolean isKing() {
//        return king;
//    }
//
//    public void kingMe() {
//        king = true;
//    }
}
