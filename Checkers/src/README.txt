
Checkers Game

 - Adjustable board size
    - No smaller than 4x4
 - Text based (for now)

Gamepiece Abstract Class / Interface
    Properties:
    1.  Piece color

    Methods:
    1.  constructor (piece color)
    2.  get possible moves (piece location)
    3.  get piece color

Checker Class
