//package com.techelevator;
//
//import org.junit.*;
//
//public class CheckerBoardTests2 {
//
//    static final int SMALL_LENGTH = 4;
//    static final int SMALL_WIDTH = 5;
//
//    private static final int MEDIUM_LENGTH = 12;
//    private static final int MEDIUM_WIDTH = 10;
//
//    private static final int LARGE_LENGTH = 22;
//    private static final int LARGE_WIDTH = 25;
//
//    //private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
//
//    private CheckerBoard smallBoard, mediumBoard, largeBoard;
//
//    @Before
//    public void setup() {
//        smallBoard = new CheckerBoard(SMALL_LENGTH, SMALL_WIDTH);
//        mediumBoard = new CheckerBoard(MEDIUM_LENGTH, MEDIUM_WIDTH);
//        largeBoard = new CheckerBoard(LARGE_LENGTH, LARGE_WIDTH);
//    }
//
//    @Test
//    public void column_a_row_1_converts_to_row_of_one_less_than_board_length_and_column_0() {
//        String boardPosition = "a1";
//
//        int[] expectedResult = new int[] {smallBoard.getNumberOfRows() - 1, 0};
//        int[] actualResult = smallBoard.squareNameToBoardArrayIndices(boardPosition);
//        Assert.assertArrayEquals(expectedResult, actualResult);
//
//        expectedResult = new int[] {mediumBoard.getNumberOfRows() - 1, 0};
//        actualResult = mediumBoard.squareNameToBoardArrayIndices(boardPosition);
//        Assert.assertArrayEquals(expectedResult, actualResult);
//
//        expectedResult = new int[] {largeBoard.getNumberOfRows() - 1, 0};
//        actualResult = largeBoard.squareNameToBoardArrayIndices(boardPosition);
//        Assert.assertArrayEquals(expectedResult, actualResult);
//    }
//
//    @Test
//    public void col_z_and_topmost_row_is_row_index_of_0_and_col_index_of_25() {
//        String boardPosition = "z" + Integer.toString(largeBoard.getNumberOfRows());
//
//        int[] expectedResult = new int[] {0, 25};
//        int[] actualResult = largeBoard.squareNameToBoardArrayIndices(boardPosition);
//        Assert.assertArrayEquals(expectedResult, actualResult);
//    }
//
//    Column char can be lowercase or uppercase
//
//
//
//}
