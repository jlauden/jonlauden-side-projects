package com.techelevator;

/*
 * Takes user input and passes it to the CLI
 * Checks user input for validity
 * Converts user moves into 2D array indices
 */

import java.util.Scanner;

public class Menu {

    // Minimum size is a property of the CheckerBoard class because a board must be at least a certain size.
    // Maximum size is determined by the menu; it will need to handle user input and this may get
    // complicated as size increases.

    public static final String COLUMN_NAMES = "abcdefghijklmnopqrstuvwxyz";
    private static final int MAXIMUM_COLUMNS = COLUMN_NAMES.length();
    private static final int MAXIMUM_ROWS = MAXIMUM_COLUMNS;
    private static final Scanner in = new Scanner(System.in);


    public void displayWelcomeMessage() {
        System.out.println();
        System.out.println("Welcome to Checkers!");
        System.out.println();
    }

    public String getStartMenuChoiceFromUser() {

        String choice = null;

        while (choice == null) {
            System.out.println("Please select from the menu below:");
            System.out.println("(1) New Game");
            System.out.println("(2) Load Game");
            System.out.println("(3) Quit");

            choice = in.nextLine();

            // Validate input
            if (choice.equals("1") || choice.equals("2") || choice.equals("3")) {
                break;
            } else {
                System.out.println("\nInvalid selection, please try again\n");
                choice = null;
            }
        }
        return choice;
    }

    public int[] getBoardSizeFromUser() {
        int[] boardSize = null;

        while (boardSize == null) {
            System.out.println("\nHow many squares wide (left to right) would you like your board to be?");
            String colNumberString = in.nextLine();
            System.out.println("\nHow many squares deep (top to bottom) would you like your board to be?");
            String rowNumberString = in.nextLine();

            // Validate input
            try {
                int rowNumber = Integer.parseInt(rowNumberString);
                int colNumber = Integer.parseInt(colNumberString);
                // If parseInt did not throw exception, check if within bounds
                if (invalidRowOrColNumber(rowNumber, colNumber)) {
                    displayInvalidRowOrColDimensionsMessage();
                } else {
                    boardSize = new int[]{rowNumber, colNumber};
                }

            } catch (NumberFormatException e) {
                System.out.println("\nIt appears there was an error converting your board dimensions.");
                System.out.println("Please make sure to enter a numeric value for the number of rows and columns.\n");
            }
        }
        return boardSize;
    }

    public void displayBoard(CheckerBoard board) {
        // TODO Change this to use formatted string with 2 char right justified row number and left justified row
        //
        String boardString = "";
        int numberOfBoardRows = board.getNumberOfRows();
        // Loop through rows
        for (int row = 0; row < numberOfBoardRows; row++) {
            // Each line begins with row number
            boardString += (rowArrayIndexToNumberString(row, numberOfBoardRows) + " |");
            // Loop through cols for current row
            for (int col = 0; col < board.getNumberOfColumns(); col++) {
                boardString += " |";
            }
            boardString += "\n";
        }
        boardString += "   ";
        for (int k = 0; k <= board.getNumberOfColumns(); k++) {
            boardString += colArrayIndexToLetterString(k) + " ";
        }
        System.out.println(boardString);
    }

    public int[] boardSquareToArrayIndices(String boardSquarePosition, int numberOfBoardRows) {
        int[] arrayIndices = new int[2];

        // Display format: "a1" --> col a row 1
        String col = boardSquarePosition.substring(0, 1);
        // row substring may be one or two digits
        String row = boardSquarePosition.substring(1);

        int rowIndex = rowNumberStringToArrayIndex(row, numberOfBoardRows);
        int colIndex = colLetterStringToArrayIndex(col);
        // Array format:  [7][0] --> row 7 col 1
        return new int[]{rowIndex, colIndex};
    }

    private int rowNumberStringToArrayIndex(String rowNumberString, int numberOfBoardRows) {
        // Changes row from 1-based descending to 0-based ascending
        int rowNumber = Integer.parseInt(rowNumberString);
        return numberOfBoardRows - rowNumber;
    }

    public int colLetterStringToArrayIndex(String columnLetter) {
        return COLUMN_NAMES.indexOf(columnLetter);
    }

    public String rowArrayIndexToNumberString(int rowIndex, int numberOfBoardRows) {
        return Integer.toString(numberOfBoardRows - rowIndex);
    }

    public String colArrayIndexToLetterString(int colIndex) {
        if (colIndex < MAXIMUM_COLUMNS) {
            return COLUMN_NAMES.substring(colIndex, colIndex + 1);
        }
        return COLUMN_NAMES.substring(colIndex);
    }

    private boolean invalidRowOrColNumber(int row, int col) {
        return row < CheckerBoard.MINIMUM_ROWS || row > MAXIMUM_ROWS ||
                col < CheckerBoard.MINIMUM_COLUMNS || col > MAXIMUM_COLUMNS;
    }

    private void displayInvalidRowOrColDimensionsMessage() {
        System.out.println("\nIt appears that you have enters a row or column dimension" +
                " that is outside what is allowed.");
        System.out.println("The number of rows must be larger than " + CheckerBoard.MINIMUM_ROWS +
                " and smaller than " + MAXIMUM_ROWS);
        System.out.println("The number of columns must be larger than " + CheckerBoard.MINIMUM_COLUMNS +
                " and smaller than " + MAXIMUM_COLUMNS);
        System.out.println("Please try again. \n");
    }

}
