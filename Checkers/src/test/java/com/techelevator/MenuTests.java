package com.techelevator;

import org.junit.*;


import java.util.ArrayList;
import java.util.List;


public class MenuTests {

    private Menu menu;

    @Before
    public void setup() {
        menu = new Menu();
    }

    @Test
    public void board_square_converts_to_correct_array_indices() {
        List<String> boardSquaresList = new ArrayList<String>();
        List<int[]> arrayIndicesList = new ArrayList<int[]>();
        int NUMBER_OF_TEST_BOARD_ROWS = 6;

        boardSquaresList.add(         "a6" );
        arrayIndicesList.add(new int[]{0,0});

        boardSquaresList.add(         "a1" );
        arrayIndicesList.add(new int[]{5,0});

        boardSquaresList.add(         "h6" );
        arrayIndicesList.add(new int[]{0,7});

        boardSquaresList.add(         "h1" );
        arrayIndicesList.add(new int[]{5,7});

        boardSquaresList.add(         "d3" );
        arrayIndicesList.add(new int[]{3,3});

        for (int i = 0; i < boardSquaresList.size(); i++) {
            Assert.assertArrayEquals(
                    "Board square " + boardSquaresList.get(i) + "converted to an unexpected value.",
                    arrayIndicesList.get(i),
                    menu.boardSquareToArrayIndices(boardSquaresList.get(i), NUMBER_OF_TEST_BOARD_ROWS)
            );
        }
    }

    @Test
    public void row_array_index_to_number_string_test() {
        int boardRows = 8;
        int arrayIndex = 0;
        String expectedRowNumber = "8";
        Assert.assertEquals("rowArrayIndexToNumberString produced an unexpected value",
                expectedRowNumber, menu.rowArrayIndexToNumberString(arrayIndex, boardRows));

        arrayIndex = 4;
        expectedRowNumber = "4";
        Assert.assertEquals("rowArrayIndexToNumberString produced an unexpected value",
                expectedRowNumber, menu.rowArrayIndexToNumberString(arrayIndex, boardRows));

        arrayIndex = boardRows - 1;
        expectedRowNumber = "1";
        Assert.assertEquals(
                "rowArrayIndexToNumberString produced a number different than what was expected",
                expectedRowNumber, menu.rowArrayIndexToNumberString(arrayIndex, boardRows)
        );

    }

    @Test
    public void col_array_index_to_letter_string_test() {
        int colArrayIndex = 0;
        String expectedLetterString = "a";
        Assert.assertEquals(
                "colArrayIndexToLetterString produced a letter that was different than the expected value",
                expectedLetterString, menu.colArrayIndexToLetterString(colArrayIndex)
                );

        // Final position in array
        colArrayIndex = menu.COLUMN_NAMES.length() - 1;
        // String containing final char of Names
        expectedLetterString = menu.COLUMN_NAMES.substring(colArrayIndex);
        Assert.assertEquals(
                "colArrayIndexToLetterString produced a letter that was different than the expected value",
                expectedLetterString, menu.colArrayIndexToLetterString(colArrayIndex)
        );
    }

}
