package com.techelevator;

import org.junit.*;

public class CheckerBoardTests {

    private static final int MINIMUM_ROWS_AND_COLUMNS = CheckerBoard.MINIMUM_COLUMNS;
    private static final int DEFAULT_ROWS_AND_COLUMNS = 8;

    private CheckerBoard checkerBoard;

    //    Max number of checkers is length/2*(height-2)
//    Gameboard should have toString representation
//    Checkers should not be able to be placed on invalid board positions
//    lower right and upper left squares can contain checkers
//    Gameboard can hold checkers
//    Gameboard has squares in rows and columns
//    checkers cannot be moved off board

    @Before
    public void setup() {
        checkerBoard = new CheckerBoard(DEFAULT_ROWS_AND_COLUMNS, DEFAULT_ROWS_AND_COLUMNS);
    }

    /*
    Board Dimensions
     */

    @Test
    public void board_dimensions_set_on_initialization() {
        Assert.assertEquals(DEFAULT_ROWS_AND_COLUMNS, checkerBoard.getNumberOfRows());
        Assert.assertEquals(DEFAULT_ROWS_AND_COLUMNS, checkerBoard.getNumberOfColumns());
    }

    @Test
    public void board_sizes_cannot_be_set_less_than_minimum() {
        int oneLessThanMin = MINIMUM_ROWS_AND_COLUMNS - 1;
        int intGreaterThanMin = 5;

        // Case in which both length and width are less than minimum
        checkerBoard = new CheckerBoard(oneLessThanMin, oneLessThanMin);
        Assert.assertEquals(MINIMUM_ROWS_AND_COLUMNS, checkerBoard.getNumberOfColumns());
        Assert.assertEquals(MINIMUM_ROWS_AND_COLUMNS, checkerBoard.getNumberOfRows());

        // Case in which length is below minimum
        checkerBoard = new CheckerBoard(oneLessThanMin, intGreaterThanMin);
        Assert.assertEquals(MINIMUM_ROWS_AND_COLUMNS, checkerBoard.getNumberOfRows());

        // Case in which width is below minimum
        checkerBoard = new CheckerBoard(intGreaterThanMin, oneLessThanMin);
        Assert.assertEquals(MINIMUM_ROWS_AND_COLUMNS, checkerBoard.getNumberOfColumns());
    }

    /*
    pickup piece tests
        if square does not exist return null
        if there is no piece on that square return null
        if there is a checker on that square, return that checker
        if there is a king on that square, return the king
     */

//    @Test
//    public void get_checker_returns_null_if_square_does_not_exist() {
//        checkerBoard = new CheckerBoard(DEFAULT_ROWS_AND_COLUMNS, DEFAULT_ROWS_AND_COLUMNS);
//        int outOfBoundIndex = DEFAULT_ROWS_AND_COLUMNS + 1;
//        Assert.assertNull(checkerBoard.getChecker(outOfBoundIndex, outOfBoundIndex));
//    }

    @Test
    public void get_checker_returns_null_if_no_piece_on_square() {
        int row = 0;
        int col = 0;
        Assert.assertNull(checkerBoard.getPiece(row, col));
    }

    @Test
    public void get_piece_returns_piece_if_one_is_on_square() {
        int row = 0;
        int col = 0;
        Checker[][] board = new Checker[DEFAULT_ROWS_AND_COLUMNS][DEFAULT_ROWS_AND_COLUMNS];
        board[row][col] = new Checker();
        checkerBoard = new CheckerBoard(board);

        Assert.assertNotNull("Expected checker object was null", checkerBoard.getPiece(row, col));
    }

    @Test
    public void remove_piece_results_in_null_at_board_square() {
        int row = 0;
        int col = 0;
        Checker[][] board = new Checker[DEFAULT_ROWS_AND_COLUMNS][DEFAULT_ROWS_AND_COLUMNS];
        board[row][col] = new Checker();
        checkerBoard = new CheckerBoard(board);

        checkerBoard.removePiece(row, col);
        Assert.assertNull("Expected board square to be null after removePiece was called",
                checkerBoard.getPiece(row, col));
    }

    @Test
    public void place_piece_adds_piece_to_target_square_if_empty() {
        int row = 0;
        int col = 0;
        Checker pieceAtSquare = new Checker();
        checkerBoard.placePiece(pieceAtSquare, row, col);
        Assert.assertEquals("Place piece appears to have failed to add a piece to an empty board square",
                pieceAtSquare, checkerBoard.getPiece(row, col));
    }

    // TODO: What happens when move piece destination is not in the board
    /*
    Move Piece Tests

    origin before   destination before  origin after    destination after
X   Checker         empty               empty           Checker
    empty                                               empty
    Checker 1       Checker 2           Checker 1       Checker 2

     */

    @Test
    public void move_piece_returns_false_if_origin_empty() {
        int originRow = 0;
        int originCol = 0;
        int destinationRow = 1;
        int destinationCol = 1;

        boolean result = checkerBoard.movePiece(originRow, originCol, destinationRow, destinationCol);

        Assert.assertFalse("movePiece did not return false when origin square was empty", result);
        Assert.assertNull(
                "Piece was added to destination but no piece was present at origin",
                checkerBoard.getPiece(destinationRow, destinationCol)
        );
    }

    @Test
    public void move_piece_returns_false_if_destination_is_occupied() {
        int originRow = 0;
        int originCol = 0;
        int destinationRow = 1;
        int destinationCol = 1;

        // Create new CheckerBoard with checkers at origin and destination squares
        Checker[][] board = new Checker[DEFAULT_ROWS_AND_COLUMNS][DEFAULT_ROWS_AND_COLUMNS];
        board[originRow][originCol] = new Checker();
        board[destinationRow][destinationCol] = new Checker();
        checkerBoard = new CheckerBoard(board);

        boolean result = checkerBoard.movePiece(originRow, originCol, destinationRow, destinationCol);

        Assert.assertFalse(
                "movePiece removed piece from origin square when destination square was occupied",
                result);
        Assert.assertNotNull(
                "movePiece should not change origin square if a gamepiece is present at the destination",
                checkerBoard.getPiece(originRow, originCol));
    }

    @Test
    public void move_piece_succeeds_when_origin_not_empty_and_destination_empty() {
        int originRow = 0;
        int originCol = 0;
        int destinationRow = 1;
        int destinationCol = 1;

        // Create new CheckerBoard with one checker at origin position
        Checker[][] board = new Checker[DEFAULT_ROWS_AND_COLUMNS][DEFAULT_ROWS_AND_COLUMNS];
        board[originRow][originCol] = new Checker();
        checkerBoard = new CheckerBoard(board);

        boolean result = checkerBoard.movePiece(originRow, originCol, destinationRow, destinationCol);

        Assert.assertNull("Origin square should be empty after piece is moved from that square",
                checkerBoard.getPiece(originRow, originCol));
        Assert.assertNotNull("Destination square should not be null after piece is moved to it",
                checkerBoard.getPiece(destinationRow, destinationCol));
        Assert.assertTrue("movePiece should return true when piece is successfully moved", result);
    }


    /*
    put down piece tests
    putting down a piece adds a checker to that square
    Attempt to put a piece on a white square returns false
     */

//    @Test
//    public void put_down_piece_returns_false_when_square_is_occupied() {
//        String squarePosition = "a1";
//        CheckerBoard checkerboard = new CheckerBoard();
//        checkerboard.putDownPiece(new Checker("Red"), squarePosition);
//        Checker checkerToPlace = new Checker("Black");
//
//        boolean result = checkerboard.putDownPiece(checkerToPlace, squarePosition);
//        Assert.assertFalse(result);
//    }
//
//    @Test
//    public void put_down_piece_does_not_replace_piece_when_square_is_occupied() {
//        CheckerBoard checkerboard = new CheckerBoard();
//        String squarePosition = "a1";
//        Checker existingChecker = new Checker("Red");
//        Checker checkerToPlace = new Checker("Black");
//
//        checkerboard.putDownPiece(existingChecker, squarePosition);
//        checkerboard.putDownPiece(checkerToPlace, squarePosition);
//        Assert.assertNotEquals(checkerboard.getPieceColor(squarePosition), checkerToPlace.getColor());
//    }
//
//    @Test
//    public void is_a_board_square_true_if_square_within_dimensions() {
//        checkerBoard = new CheckerBoard(10, 6);
//        Assert.assertTrue(checkerBoard.isABoardSquare("a1"));
//        Assert.assertTrue(checkerBoard.isABoardSquare("b1"));
//        Assert.assertTrue(checkerBoard.isABoardSquare("f9"));
//    }
//
//    @Test
//    public void is_a_board_square_false_if_square_outside_dimensions() {
//        checkerBoard = new CheckerBoard();
//        Assert.assertFalse(checkerBoard.isABoardSquare("i8"));
//        Assert.assertFalse(checkerBoard.isABoardSquare("h9"));
//        Assert.assertFalse(checkerBoard.isABoardSquare("g12"));
//    }


}

