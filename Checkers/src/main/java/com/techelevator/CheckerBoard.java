package com.techelevator;

public class CheckerBoard {

    public static final int MINIMUM_ROWS = 4;
    public static final int MINIMUM_COLUMNS = 4;

    private Checker[][] board;

    public CheckerBoard(int numberOfRows, int numberOfColumns) {
        int rows = validateRowDimension(numberOfRows);
        int cols = validateColDimension(numberOfColumns);

        board = new Checker[rows][cols];
    }

    // Constructor accepting a pre-built Checker array to allow independent unit test
    public CheckerBoard(Checker[][] board) {
        if (board.length < MINIMUM_ROWS || board[0].length < MINIMUM_COLUMNS) {
            throw new ArrayIndexOutOfBoundsException(
                    "CheckerBoard may not be constructed with a 2D array that is smaller than minimum values"
            );
        }
        this.board = board;
    }

    public boolean placePiece(Checker pieceToPlace, int row, int col){
        if (board[row][col] == null){
            board[row][col] = pieceToPlace;
            return true;
        }
        return false;
    }

    public void removePiece(int row, int col) {
        board[row][col] = null;
    }

    /**
     * Changes the configuration of the CheckerBoard by removing a piece from an origin position and
     * placing a new equivalent piece at the destination position.  A boolean value is returned to indicate
     * whether the move was successful.  Row and Column positions are ordered ascending top to bottom and
     * left to right.
     *
     * @param originRow         The row from which the piece will be moved
     * @param originCol         The column from which the piece will be moved
     * @param destinationRow    The row to which the piece will be moved
     * @param destinationCol    The column to which the piece will be moved
     * @return
     */
    public boolean movePiece(int originRow, int originCol, int destinationRow, int destinationCol) {
        // if piece at origin and no piece at destination, set origin to null and destination to piece
        if (board[destinationRow][destinationCol] == null && board[originRow][originCol] != null) {
            board[destinationRow][destinationCol] = new Checker(board[originRow][originCol]);
            board[originRow][originCol] = null;
            return true;
        }
        return false;
    }

    public Checker getPiece(int row, int col) {
        return board[row][col];
    }

    public int getNumberOfColumns() {
        return board[0].length;
    }

    public int getNumberOfRows() {
        return board.length;
    }

    public int getLastRowIndex() {
        return getNumberOfRows() - 1;
    }

    public int getLastColumnIndex() {
        return getNumberOfColumns() - 1;
    }

    private int validateRowDimension(int rows) {
        return Integer.max(rows, MINIMUM_ROWS);
    }

    private int validateColDimension(int cols) {
        return Integer.max(cols, MINIMUM_COLUMNS);
    }
}


//    public int[] squareNameToBoardArrayIndices(String squareName){
//        // Display format "a1" (col, row)
//        String col = squareName.substring(0, 1);
//        // row is index 1 to end, thus including single and double digit rows
//        String row = squareName.substring(1);
//
//        int rowIndex = rowDisplayNameToArrayIndex(row);
//        int colIndex = colDisplayNameToArrayIndex(col);
//        // Array format [7][0] (row, col)
//        return new int[] {rowIndex, colIndex};
//    }

//    public int rowDisplayNameToArrayIndex(String displayedName){
//        int displayedNumber = Integer.parseInt(displayedName);
//        // results in top row being 0, increasing from top to bottom
//        return numberOfRows - displayedNumber;
//    }
//
//    public int colDisplayNameToArrayIndex(String columnLetter){
//        return COLUMN_NAMES.indexOf(columnLetter);
//    }

//    public boolean isABoardSquare(String squareName) {
//        int[] squareIndices = squareNameToBoardArrayIndices(squareName);
//        int rowIndex = squareIndices[0];
//        int colIndex = squareIndices[1];
//
//        if (rowIndex < numberOfRows && rowIndex >= 0
//                && colIndex < numberOfColumns && colIndex >= 0) {
//            return true;
//        }
//        return false;
//    }

//    public String getPieceColor(String squareName) {
//        int[] squareIndices = squareNameToBoardArrayIndices(squareName);
//        int rowIndex = squareIndices[0];
//        int colIndex = squareIndices[1];
//
//        return board[rowIndex][colIndex].getColor();
//    }



//    private void placeCheckersInStartingPositions() {
//
//    }

//    @Override
//    public String toString() {
//        String board = "";
//        for (int row : rowReferenceIntegers) {
//            board += row + " |";
//            for (char column : columnReferenceCharacters) {
//                board += " |";
//            }
//            board += "\n";
//        }
//        board += "   ";
//        for (char column : columnReferenceCharacters){
//            board += column + " ";
//        }
//        return board;
//    }
//}
//
//
//
//


/*

BOARD REPRESENTATIONS

8 |  [x] [x] [x] [x]|
7 |[x] [x] [x] [x]  |
6 |  [x] [x] [x] [x]|
5 |[ ] [ ] [ ] [ ]  |
4 |  [ ] [ ] [ ] [ ]|
3 |[0] [0] [0] [0]  |
2 |  [0] [0] [0] [0]|
1 |[0] [0] [0] [0]  |
    A B C D E F G H
    a b c d e f g h


8 | |x| |x| |x| |x|
7 |x| |x| |x| |x| |
6 | |x| |x| |x| |x|
5 | | | | | | | | |
4 | | | | | | | | |
3 |o| |o| |o| |o| |
2 | |o| |o| |o| |o|
1 |o| |o| |o| |o| |
   A B C D E F G H

    A B C D E F G H
8 |  (x) (x) (x) (x)| 8
7 |(x) (x) (x) (x)  | 7
6 |  (x) (x) (x) (x)| 6
5 |( ) ( ) ( ) ( )  | 5
4 |  ( ) ( ) ( ) ( )| 4
3 |(o) (o) (o) (o)  | 3
2 |  (o) (o) (o) (o)| 2
1 |(o) (o) (o) (o)  | 1
    A B C D E F G H

*/
