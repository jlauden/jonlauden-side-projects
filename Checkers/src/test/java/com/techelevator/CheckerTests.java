package com.techelevator;

import org.junit.*;

public class CheckerTests {

    private Checker checker;
    private static final String START_COLOR = "Red";

    @Before
    public void setup() {
        checker = new Checker(START_COLOR);
    }

    @Test
    public void checker_is_instantiated_with_color() {
        Assert.assertEquals(START_COLOR, checker.getColor());
    }


//    @Test
//    public void checker_does_not_start_as_king() {
//        Assert.assertFalse(checker.isKing());
//    }

/*
TODO
    Color may not be null
    Color may not be empty
 */

}
