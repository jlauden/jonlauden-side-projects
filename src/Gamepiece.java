public class Gamepiece {

    private char horizontalPosition;
    private int verticalPosition;
    private final String color;

    public Gamepiece(char horizontalPosition, int verticalPosition, String color) {
        this.horizontalPosition = horizontalPosition;
        this.verticalPosition = verticalPosition;
        this.color = color;
    }

    public String getPosition() {
        return Character.toString(horizontalPosition) + verticalPosition;
    }

    public boolean moveTo(String newPosition) {
        if (newPosition.length() > 2){
            return false;
        }
        horizontalPosition = newPosition.charAt(0);
        verticalPosition = Integer.parseInt(newPosition.substring(1));
        return true;
    }

    public char getHorizontalPosition() {
        return horizontalPosition;
    }

    public int getVerticalPosition() {
        return verticalPosition;
    }

    public String getColor() {
        return color;
    }
}
