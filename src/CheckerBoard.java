public class CheckerBoard {

    private final char[] columnReferenceCharacters;
    private final int[] rowReferenceIntegers;

    public CheckerBoard() {
        columnReferenceCharacters = new char[] {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};
        rowReferenceIntegers = new int[] {1, 2, 3, 4, 5, 6, 7, 8};
    }

    @Override
    public String toString() {
        String board = "";
        for (int row : rowReferenceIntegers) {
            board += row + " |";
            for (char column : columnReferenceCharacters) {
                board += " |";
            }
        }
        for (char column : columnReferenceCharacters){
            board += column + " ";
        }
        return board;
    }
}
//
//
//
//


/*

BOARD REPRESENTATIONS

8 |  [x] [x] [x] [x]|
7 |[x] [x] [x] [x]  |
6 |  [x] [x] [x] [x]|
5 |[ ] [ ] [ ] [ ]  |
4 |  [ ] [ ] [ ] [ ]|
3 |[0] [0] [0] [0]  |
2 |  [0] [0] [0] [0]|
1 |[0] [0] [0] [0]  |
    A B C D E F G H
    a b c d e f g h


8 | |x| |x| |x| |x|
7 |x| |x| |x| |x| |
6 | |x| |x| |x| |x|
5 | | | | | | | | |
4 | | | | | | | | |
3 |o| |o| |o| |o| |
2 | |o| |o| |o| |o|
1 |o| |o| |o| |o| |
   A B C D E F G H

    A B C D E F G H
8 |  (x) (x) (x) (x)| 8
7 |(x) (x) (x) (x)  | 7
6 |  (x) (x) (x) (x)| 6
5 |( ) ( ) ( ) ( )  | 5
4 |  ( ) ( ) ( ) ( )| 4
3 |(o) (o) (o) (o)  | 3
2 |  (o) (o) (o) (o)| 2
1 |(o) (o) (o) (o)  | 1
    A B C D E F G H

*/
